import string
import soundfile as sf
import numpy as np
import torch
import pathlib

from flask import Flask, jsonify, make_response, request, render_template


temp = pathlib.PosixPath
pathlib.PosixPath = pathlib.WindowsPath

app = Flask(__name__, template_folder='template')


PATH = "lol.pth"
model = torch.load(PATH, map_location=torch.device('cpu'))

def text_normalizer(text):
    text = text.upper()
    return text.translate(str.maketrans('', '', string.punctuation))

@app.route("/i")
def home():
    return "hello world"

@app.route("/", methods=["POST","GET"])
def predict():
    text = ""
    fs = 16000

    if request.method=="POST":
        if "file" not in request.files:
            return "blom ke isi"
        file = request.files["file"]

        if file.filename=="":
            return "file nya gk ketemu"
        if file:
            audio, rate = sf.read(file)
            text = model(audio)[0]
            text = text_normalizer(text)

            
    return render_template("upload.html", text = text)


if __name__ == "__main__":
    app.run()